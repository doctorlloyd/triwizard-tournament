package com.hogwarts.triwizardtournament.locals

import android.content.Context
import android.content.SharedPreferences

class Preference(context: Context) {

    private val KEY = "KEY"
    private val CHARACTER_NAME = "NAME"
    private val CHARACTER = "CHARACTER"

    private val preference: SharedPreferences =
        context.getSharedPreferences(KEY, Context.MODE_PRIVATE)
    private val editor = preference.edit()

    fun setName(name: String) {
        editor.putString(CHARACTER_NAME, name)
        editor.apply()
    }

    fun getName(): String {
        return preference.getString(CHARACTER_NAME, CHARACTER_NAME)!!
    }

    fun setCharacter(character:Set<String>){
        editor.putStringSet(CHARACTER,character)
    }
}