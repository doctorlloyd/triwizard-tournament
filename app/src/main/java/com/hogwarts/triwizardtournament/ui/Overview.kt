package com.hogwarts.triwizardtournament.ui

import android.content.ContentValues
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.hogwarts.triwizardtournament.R
import com.hogwarts.triwizardtournament.locals.Preference
import com.hogwarts.triwizardtournament.models.Characters
import com.hogwarts.triwizardtournament.models.Houses
import com.hogwarts.triwizardtournament.models.Spells
import kotlinx.android.synthetic.main.characters_layout.*
import kotlinx.android.synthetic.main.house_card_view.*
import kotlinx.android.synthetic.main.houses_layout.*
import kotlinx.android.synthetic.main.houses_layout.founder
import kotlinx.android.synthetic.main.houses_layout.headOfHouse
import kotlinx.android.synthetic.main.houses_layout.houseGhost
import kotlinx.android.synthetic.main.spells_layout.*
import kotlinx.android.synthetic.main.spells_layout.spell
import java.lang.Exception

class Overview : AppCompatActivity() {
    private lateinit var objectReceived: Any
    private lateinit var preference: Preference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile)
        preference = Preference(this)

        try {
            objectReceived = intent.getSerializableExtra("object") as Any
            when (objectReceived) {
                is Characters -> {
                    characters_layout.visibility = View.VISIBLE
                    title = getString(R.string.character_title)
                    characters(objectReceived as Characters)
                }
                is Houses -> {
                    houses_layout.visibility = View.VISIBLE
                    title = getString(R.string.house_title)
                    houses(objectReceived as Houses)
                }
                is Spells -> {
                    spells_layout.visibility = View.VISIBLE
                    title = getString(R.string.spell_title)
                    spells(objectReceived as Spells)
                }
            }
        } catch (e: Exception) {
            Log.d(ContentValues.TAG, "Error: " + e.localizedMessage)
        }
    }

    private fun characters(characters: Characters){
        characters_id.text = characters.id
        character_name.text = characters.name
        characters_name.text = characters.name
        characters_role.text = characters.role
        characters_house.text = characters.house
        characters_school.text = characters.school
        species.text = characters.species
        bloodStatus.text = characters.bloodStatus
    }
    private fun spells(spells: Spells){
        spell_id.text = spells._id
        spell.text = spells.spell
        spell_name.text = spells.spell
        spell_type.text = spells.type
        spell_effect.text = spells.effect
    }
    private fun houses(houses: Houses){
        house_id.text = houses._id
        house_mascot.text = houses.mascot
        houseGhost.text =  houses.houseGhost
        houses_name.text = houses.name
        house_name.text = houses.name
        houses_school.text = houses.school
        headOfHouse.text = houses.headOfHouse
        members.text = houses.members.size.toString()
        founder.text = houses.founder
    }
}
