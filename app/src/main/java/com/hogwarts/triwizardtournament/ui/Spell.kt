package com.hogwarts.triwizardtournament.ui

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hogwarts.triwizardtournament.R
import com.hogwarts.triwizardtournament.adapters.HouseAdapter
import com.hogwarts.triwizardtournament.adapters.SpellAdapter
import com.hogwarts.triwizardtournament.models.Houses
import com.hogwarts.triwizardtournament.models.Spells
import com.hogwarts.triwizardtournament.network.ServiceManager
import com.hogwarts.triwizardtournament.ui.support.DialogLayout
import kotlinx.android.synthetic.main.recycler_view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Spell : AppCompatActivity() {
    private lateinit var dialog: DialogLayout
    private var routes: ServiceManager = ServiceManager()
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var spellAdapter: SpellAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spell)
        dialog = DialogLayout(this)
        create()

        layoutManager = LinearLayoutManager(this)
        search_recycler_view.layoutManager = layoutManager

        getSpells()

        swipeToRefresh.setOnRefreshListener {
            getSpells()
            swipeToRefresh.isRefreshing = false
        }
    }

    private fun getSpells() {
        if (!dialog.active())
            dialog.show()

        routes.getSpells().enqueue(object : Callback<List<Spells>> {
            override fun onResponse(call: Call<List<Spells>>, response: Response<List<Spells>>) {
                if (response.isSuccessful) {
                    val spells = response.body()!!
                    dialog.hideDialog()

                    spellAdapter = SpellAdapter(this@Spell, spells)
                    search_recycler_view.adapter = spellAdapter

                } else {
                    Log.i("Error: ", getString(R.string.errorOnCall))
                    dialog.hideDialog()
                }
            }

            override fun onFailure(call: Call<List<Spells>>, t: Throwable) {
                Log.i("Error: ", "${t.message}")
                dialog.hideDialog()
            }
        })
    }

    private fun create() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.createDialog()
        }
    }
}
