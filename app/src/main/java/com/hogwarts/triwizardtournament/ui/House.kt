package com.hogwarts.triwizardtournament.ui

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hogwarts.triwizardtournament.R
import com.hogwarts.triwizardtournament.adapters.HouseAdapter
import com.hogwarts.triwizardtournament.models.Houses
import com.hogwarts.triwizardtournament.network.ServiceManager
import com.hogwarts.triwizardtournament.ui.support.DialogLayout
import kotlinx.android.synthetic.main.activity_houses.*
import kotlinx.android.synthetic.main.characters.*
import kotlinx.android.synthetic.main.recycler_view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class House : AppCompatActivity() {
    private lateinit var dialog: DialogLayout
    private var routes: ServiceManager = ServiceManager()
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var houseAdapter: HouseAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_houses)
        dialog = DialogLayout(this)
        create()

        layoutManager = LinearLayoutManager(this)
        search_recycler_view.layoutManager = layoutManager

        getHouses()

        swipeToRefresh.setOnRefreshListener {
            getHouses()
            swipeToRefresh.isRefreshing = false
        }

        search_house.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                //TODO add timer for word change
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                getHouse(query)
                return false
            }
        })
    }

    private fun getHouse(id: String) {
        if (!dialog.active())
            dialog.show()

        routes.getHouse(id).enqueue(object : Callback<Houses> {
            override fun onResponse(call: Call<Houses>, response: Response<Houses>) {
                if (response.isSuccessful) {
                    val house = response.body()!!
                    dialog.hideDialog()
                    val intent = Intent(this@House, Overview::class.java)
                    intent.putExtra("object", house)
                    startActivity(intent)
                } else {
                    getString(R.string.errorOnCall)
                    dialog.hideDialog()
                }
            }

            override fun onFailure(call: Call<Houses>, t: Throwable) {
                Log.i("Error: ", "${t.message}")
                dialog.hideDialog()
            }
        })
    }

    private fun getHouses() {
        if (!dialog.active())
            dialog.show()

        routes.getHouses().enqueue(object : Callback<List<Houses>> {
            override fun onResponse(call: Call<List<Houses>>, response: Response<List<Houses>>) {
                if (response.isSuccessful) {
                    val houses = response.body()!!
                    dialog.hideDialog()

                    houseAdapter = HouseAdapter(this@House, houses)
                    search_recycler_view.adapter = houseAdapter

                } else {
                    Log.i("Error: ", getString(R.string.errorOnCall))
                    dialog.hideDialog()
                }
            }

            override fun onFailure(call: Call<List<Houses>>, t: Throwable) {
                Log.i("Error: ", "${t.message}")
                dialog.hideDialog()
            }
        })
    }

    private fun create() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.createDialog()
        }
    }
}
