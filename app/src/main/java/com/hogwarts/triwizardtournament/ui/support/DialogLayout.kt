package com.hogwarts.triwizardtournament.ui.support

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.os.Build
import android.view.ContextThemeWrapper
import androidx.annotation.RequiresApi
import com.hogwarts.triwizardtournament.R


class DialogLayout(private val context: Context) {
    private lateinit var progressDialog: AlertDialog
    private var alertBuilder = AlertDialog.Builder(context)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun createDialog() {
        val builder = AlertDialog.Builder(ContextThemeWrapper(context as Activity, R.style.AlertDialogTheme))
        builder.setView(R.layout.fetching_data_layout)
        builder.setCancelable(false)
        progressDialog = builder.create()
        progressDialog.window!!.setBackgroundDrawableResource(R.color.transparent_background)
    }

    fun show(){
        progressDialog.show()
    }

    fun active():Boolean{
        return progressDialog.isShowing
    }

    fun hideDialog() {
        progressDialog.dismiss()
    }
}