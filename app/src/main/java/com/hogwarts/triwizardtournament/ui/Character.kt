package com.hogwarts.triwizardtournament.ui

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hogwarts.triwizardtournament.R
import com.hogwarts.triwizardtournament.adapters.CharacterAdapter
import com.hogwarts.triwizardtournament.models.Characters
import com.hogwarts.triwizardtournament.network.ServiceManager
import com.hogwarts.triwizardtournament.ui.support.DialogLayout
import kotlinx.android.synthetic.main.characters.*
import kotlinx.android.synthetic.main.recycler_view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Character : AppCompatActivity() {
    private lateinit var dialog: DialogLayout
    private var routes: ServiceManager = ServiceManager()
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var characterAdapter: CharacterAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.characters)
        dialog = DialogLayout(this)
        create()

        layoutManager = LinearLayoutManager(this)
        search_recycler_view.layoutManager = layoutManager

        getCharacters()

        swipeToRefresh.setOnRefreshListener {
            getCharacters()
            swipeToRefresh.isRefreshing = false
        }

        search_character.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                //TODO add timer for word change
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                getCharacter(query)
                return false
            }
        })
    }

    private fun getCharacters() {
        if (!dialog.active())
            dialog.show()

        routes.getCharacters().enqueue(object : Callback<List<Characters>> {
            override fun onResponse(
                call: Call<List<Characters>>,
                response: Response<List<Characters>>
            ) {
                if (response.isSuccessful) {
                    val characters = response.body()!!
                    dialog.hideDialog()

                    characterAdapter = CharacterAdapter(this@Character, characters)
                    search_recycler_view.adapter = characterAdapter

                } else {
                    getString(R.string.errorOnCall)
                    dialog.hideDialog()
                }
            }

            override fun onFailure(call: Call<List<Characters>>, t: Throwable) {
                Log.i("Error: ", "${t.message}")
                dialog.hideDialog()
            }
        })
    }

    private fun getCharacter(id: String) {
        if (!dialog.active())
            dialog.show()

        routes.getCharacter(id).enqueue(object : Callback<Characters> {
            override fun onResponse(call: Call<Characters>, response: Response<Characters>) {
                if (response.isSuccessful) {
                    val character = response.body()!!
                    dialog.hideDialog()
                    val intent = Intent(this@Character, Overview::class.java)
                    intent.putExtra("object", character)
                    startActivity(intent)
                } else {
                    getString(R.string.errorOnCall)
                    dialog.hideDialog()
                }
            }

            override fun onFailure(call: Call<Characters>, t: Throwable) {
                Log.i("Error: ", "${t.message}")
                dialog.hideDialog()
            }
        })
    }

    private fun create() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.createDialog()
        }
    }
}
