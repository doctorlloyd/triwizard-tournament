package com.hogwarts.triwizardtournament.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.utils.ColorTemplate
import com.hogwarts.triwizardtournament.R
import com.hogwarts.triwizardtournament.models.Characters
import com.hogwarts.triwizardtournament.models.Houses
import com.hogwarts.triwizardtournament.models.Spells
import com.hogwarts.triwizardtournament.network.ServiceManager
import com.hogwarts.triwizardtournament.ui.support.DialogLayout
import kotlinx.android.synthetic.main.home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Home : AppCompatActivity() {

    private lateinit var dialog: DialogLayout
    private var routes: ServiceManager = ServiceManager()
    private val noOfCharacters = ArrayList<Entry>()
    private val label = ArrayList<String>()
    private lateinit var dataSet: PieDataSet
    private var professor = 0
    private var student = 0
    private var author = 0
    private var head = 0
    private var other = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home)
        dialog = DialogLayout(this)
        create()
        createPieChart()
        getSpells()
        getHouses()

        charactersCard.setOnClickListener {
            startActivity(Intent(this@Home, Character::class.java))
        }
        housesCard.setOnClickListener {
            startActivity(Intent(this@Home, House::class.java))
        }
        spellsCard.setOnClickListener {
            startActivity(Intent(this@Home, Spell::class.java))
        }
    }

    private fun createPieChart() {

        noOfCharacters.add(Entry(945f, 0))
        noOfCharacters.add(Entry(1040f, 1))
        noOfCharacters.add(Entry(1133f, 2))
        noOfCharacters.add(Entry(1240f, 3))
        noOfCharacters.add(Entry(2369f, 4))

        label.add("Professor")
        label.add("student")
        label.add("Author")
        label.add("Head")
        label.add("Other")
        getCharacters()
    }

    private fun getCharacters() {
        if (!dialog.active())
            dialog.show()

        routes.getCharacters().enqueue(object : Callback<List<Characters>> {
            override fun onResponse(
                call: Call<List<Characters>>,
                response: Response<List<Characters>>
            ) {
                if (response.isSuccessful) {
                    val characters = response.body()!!
                    val pieChart: PieChart = findViewById(R.id.charactersPieChart)

                    for (character in characters) {
                        if (character.role != null) {
                            when {
                                "Professor" in character.role -> {
                                    noOfCharacters.add(
                                        0,
                                        Entry(professor++.toFloat(), 0)
                                    )
                                    noOfCharacters.removeAt(1)
                                }
                                "student" in character.role -> {
                                    noOfCharacters.add(
                                        1,
                                        Entry(student++.toFloat(), 1)
                                    )
                                    noOfCharacters.removeAt(2)
                                }
                                "Author" in character.role -> {
                                    noOfCharacters.add(
                                        2,
                                        Entry(author++.toFloat(), 2)
                                    )
                                    noOfCharacters.removeAt(3)
                                }
                                "Head" in character.role -> {
                                    noOfCharacters.add(
                                        3,
                                        Entry(head++.toFloat(), 3)
                                    )
                                    noOfCharacters.removeAt(4)
                                }
                                else -> {
                                    noOfCharacters.add(4, Entry(other++.toFloat(), 4))
                                    noOfCharacters.removeAt(5)
                                }
                            }
                        } else {
                            noOfCharacters.add(4, Entry(other++.toFloat(), 4))
                            noOfCharacters.removeAt(5)
                        }
                    }
                    dataSet = PieDataSet(noOfCharacters, "")

                    val data = PieData(label, dataSet)
                    pieChart.data = data
                    dataSet.setColors(ColorTemplate.JOYFUL_COLORS)
                    pieChart.animateXY(5000, 5000)

                    dialog.hideDialog()

                } else {
                    getString(R.string.errorOnCall)
                    dialog.hideDialog()
                }
            }

            override fun onFailure(call: Call<List<Characters>>, t: Throwable) {
                Log.i("Error: ","${t.message}")
                dialog.hideDialog()
            }
        })
    }

    private fun create() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.createDialog()
        }
    }

    private fun getSpells() {
        if (!dialog.active())
            dialog.show()

        routes.getSpells().enqueue(object : Callback<List<Spells>> {
            override fun onResponse(call: Call<List<Spells>>, response: Response<List<Spells>>) {
                if (response.isSuccessful) {
                    val spells = response.body()!!
                    dialog.hideDialog()
                    numberOfSpells.text = "Total number of spells: ${spells.size}"
                } else {
                    Log.i("Error: ", getString(R.string.errorOnCall))
                    dialog.hideDialog()
                }
            }

            override fun onFailure(call: Call<List<Spells>>, t: Throwable) {
                Log.i("Error: ", "${t.message}")
                dialog.hideDialog()
            }
        })
    }

    private fun getHouses() {
        if (!dialog.active())
            dialog.show()

        routes.getHouses().enqueue(object : Callback<List<Houses>> {
            override fun onResponse(call: Call<List<Houses>>, response: Response<List<Houses>>) {
                if (response.isSuccessful) {
                    val houses = response.body()!!
                    dialog.hideDialog()
                    val entries = ArrayList<BarEntry>()
                    val houseNames = ArrayList<String>()
                    for(house in houses){
                        entries.add(BarEntry(house.members.size.toFloat(),houses.indexOf(house)))
                        houseNames.add(house.name)
                    }

                    val barDataSet = BarDataSet(entries, "Members per house")
                    housesBarChart.animateY(5000)
                    val data = BarData(houseNames, barDataSet)
                    barDataSet.setColors(ColorTemplate.COLORFUL_COLORS)
                    housesBarChart.data = data

                } else {
                    Log.i("Error: ", getString(R.string.errorOnCall))
                    dialog.hideDialog()
                }
            }

            override fun onFailure(call: Call<List<Houses>>, t: Throwable) {
                Log.i("Error: ", "${t.message}")
                dialog.hideDialog()
            }
        })
    }

}
