package com.hogwarts.triwizardtournament.adapters
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.hogwarts.triwizardtournament.R
import com.hogwarts.triwizardtournament.models.Characters
import com.hogwarts.triwizardtournament.ui.Overview

class CharacterAdapter(private val context: Context, private val characters: List<Characters>) :
    RecyclerView.Adapter<CharacterAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private var characterCardView: CardView = itemView.findViewById(R.id.characterCardView)
        var name:TextView = itemView.findViewById(R.id.name)
        var role:TextView = itemView.findViewById(R.id.role)
        var house:TextView = itemView.findViewById(R.id.house)
        var school:TextView = itemView.findViewById(R.id.school)
        var species:TextView = itemView.findViewById(R.id.species)

        init {
            characterCardView.setOnClickListener { view: View ->
                val intent = Intent(context, Overview::class.java)
                intent.putExtra("object", characters[adapterPosition])
                view.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.character_card_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return characters.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val character = characters[position]
        holder.name.text = character.name
        holder.role.text = character.role
        holder.house.text = character.house
        holder.school.text = character.school
        holder.species.text = character.species
    }

}