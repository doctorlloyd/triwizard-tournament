package com.hogwarts.triwizardtournament.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.hogwarts.triwizardtournament.R
import com.hogwarts.triwizardtournament.models.Houses
import com.hogwarts.triwizardtournament.ui.Overview

class HouseAdapter (private val context: Context, private val houses: List<Houses>) :
    RecyclerView.Adapter<HouseAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private var houseCardView: CardView = itemView.findViewById(R.id.houseCardView)
        var name: TextView = itemView.findViewById(R.id.name)
        var headOfHouse: TextView = itemView.findViewById(R.id.headOfHouse)
        var houseGhost: TextView = itemView.findViewById(R.id.houseGhost)
        var school: TextView = itemView.findViewById(R.id.school)
        var founder: TextView = itemView.findViewById(R.id.founder)

        init {
            houseCardView.setOnClickListener { view: View ->
                val intent = Intent(context, Overview::class.java)
                intent.putExtra("object", houses[adapterPosition])
                view.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.house_card_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return houses.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val house = houses[position]
        holder.name.text = house.name
        holder.headOfHouse.text = house.headOfHouse
        holder.houseGhost.text = house.houseGhost
        holder.school.text = house.school
        holder.founder.text = house.founder
    }

}