package com.hogwarts.triwizardtournament.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.hogwarts.triwizardtournament.R
import com.hogwarts.triwizardtournament.models.Spells
import com.hogwarts.triwizardtournament.ui.Overview

class SpellAdapter (private val context: Context, private val spells: List<Spells>) :
    RecyclerView.Adapter<SpellAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private var spellCardView: CardView = itemView.findViewById(R.id.spellCardView)
        var spell: TextView = itemView.findViewById(R.id.spell)
        var typeOfSpell: TextView = itemView.findViewById(R.id.typeOfSpell)
        var spellEffects: TextView = itemView.findViewById(R.id.spellEffects)

        init {
            spellCardView.setOnClickListener { view: View ->
                val intent = Intent(context, Overview::class.java)
                intent.putExtra("object", spells[adapterPosition])
                view.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.spell_card_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return spells.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val spell = spells[position]
        holder.spell.text = spell.spell
        holder.typeOfSpell.text = spell.type
        holder.spellEffects.text = spell.effect
    }

}