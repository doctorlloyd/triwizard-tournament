package com.hogwarts.triwizardtournament.network

import com.hogwarts.triwizardtournament.models.Characters
import com.hogwarts.triwizardtournament.models.Houses
import com.hogwarts.triwizardtournament.models.Spells
import retrofit2.Call
import retrofit2.http.*

interface Routes {

    @GET("characters")
    fun getCharacters(
        @Query("key") key: String
    ): Call<List<Characters>>

    @GET("characters/{characterId}")
    fun getCharacter(
        @Query("key") key: String,
        @Path("characterId") characterId: String
    ): Call<Characters>

    @GET("houses/{houseId}")
    fun getHouse(
        @Query("key") key: String,
        @Path("houseId") houseId: String
    ): Call<Houses>

    @GET("houses")
    fun getHouses(
        @Query("key") key: String
    ): Call<List<Houses>>

    @GET("spells")
    fun getSpells(
        @Query("key") key: String
    ): Call<List<Spells>>

}