package com.hogwarts.triwizardtournament.network

import com.github.simonpercic.oklog3.OkLogInterceptor
import com.hogwarts.triwizardtournament.models.Characters
import com.hogwarts.triwizardtournament.models.Houses
import com.hogwarts.triwizardtournament.models.Spells
import com.hogwarts.triwizardtournament.network.interceptors.LoggingInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ServiceManager() {
    private val apiKey = "\$2a\$10\$1JEnmtEF417yBaFZcr51qukRjaKv8d5toEG5DKP/IUZWIVwfsaF7y"
    private val uriPattern = "https://www.potterapi.com/v1/"
    
    private fun getHttpClient(): OkHttpClient {
        return getHttpClientBuilder()
            .connectTimeout(500, TimeUnit.SECONDS)
            .writeTimeout(500, TimeUnit.SECONDS)
            .readTimeout(500, TimeUnit.SECONDS)
            .build()
    }

    private fun getHttpClientBuilder(): OkHttpClient.Builder {
        val httpClientBuilder = OkHttpClient.Builder()

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        httpClientBuilder.addInterceptor(LoggingInterceptor())
        httpClientBuilder.addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BODY))
        httpClientBuilder.addInterceptor(
            OkLogInterceptor.builder()
                .withRequestHeaders(true)
                .withRequestBody(true)
                .withRequestBodyState(true)
                .withResponseHeaders(true)
                .withResponseBodyState(true)
                .withRequestContentType(true)
                .withRequestContentLength(true)
                .withResponseMessage(true)
                .withAllLogData()
                .build()
        )
        return httpClientBuilder
    }

    private fun getService(): Routes {
        return getRetrofit().create(Routes::class.java)
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(uriPattern)
            .client(getHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getCharacters(): Call<List<Characters>> {
        return getService().getCharacters(apiKey)
    }

    fun getCharacter(id:String): Call<Characters>{
        return getService().getCharacter(apiKey,id)
    }

    fun getHouse(id:String): Call<Houses>{
        return getService().getHouse(apiKey,id)
    }

    fun getHouses(): Call<List<Houses>>{
        return getService().getHouses(apiKey)
    }

    fun getSpells(): Call<List<Spells>>{
        return getService().getSpells(apiKey)
    }

}