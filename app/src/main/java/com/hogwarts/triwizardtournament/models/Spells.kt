package com.hogwarts.triwizardtournament.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Spells (
	@SerializedName("_id") val _id : String,
	@SerializedName("spell") val spell : String,
	@SerializedName("type") val type : String,
	@SerializedName("effect") val effect : String
): Serializable