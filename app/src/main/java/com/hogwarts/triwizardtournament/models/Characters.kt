package com.hogwarts.triwizardtournament.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Characters (

	@SerializedName("_id") val id : String,
	@SerializedName("name") val name : String,
	@SerializedName("role") val role : String?,
	@SerializedName("house") val house : String,
	@SerializedName("school") val school : String,
	@SerializedName("__v") val __v : Int,
	@SerializedName("ministryOfMagic") val ministryOfMagic : Boolean,
	@SerializedName("orderOfThePhoenix") val orderOfThePhoenix : Boolean,
	@SerializedName("dumbledoresArmy") val dumbledoresArmy : Boolean,
	@SerializedName("deathEater") val deathEater : Boolean,
	@SerializedName("bloodStatus") val bloodStatus : String,
	@SerializedName("species") val species : String
):Serializable