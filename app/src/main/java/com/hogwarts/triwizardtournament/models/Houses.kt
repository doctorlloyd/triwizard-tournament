package com.hogwarts.triwizardtournament.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Houses (

	@SerializedName("_id") val _id : String,
	@SerializedName("name") val name : String,
	@SerializedName("mascot") val mascot : String,
	@SerializedName("headOfHouse") val headOfHouse : String,
	@SerializedName("houseGhost") val houseGhost : String,
	@SerializedName("founder") val founder : String,
	@SerializedName("__v") val __v : Int,
	@SerializedName("school") val school : String,
	@SerializedName("members") val members : List<String>,
	@SerializedName("values") val values : List<String>,
	@SerializedName("colors") val colors : List<String>
): Serializable